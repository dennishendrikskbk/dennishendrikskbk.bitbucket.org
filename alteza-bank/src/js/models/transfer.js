/* eslint-disable class-methods-use-this */
import { Model } from 'backbone';
// import Store from 'backbone.localstorage';

class TransferModel extends Model {
    get storeName() {
        return 'transfersDB';
    }

    get url() {
        return '/';
    }
}

export default TransferModel;

/* eslint-disable class-methods-use-this */
import Backbone from 'backbone';
import Router from './router';

import NavigationView from './views/navigation';

class Application {
    get loadViews() {
        return [
            NavigationView,
        ];
    }

    constructor() {
        this.setupViews();
        this.setupRouter();
    }

    setupViews() {
        this.activeViews = [];
        this.loadViews.forEach((ViewClass) => {
            const instance = new ViewClass({
                el: this.el,
                component: this,
            });
            this.activeViews.push(instance);
        });
    }

    /**
     * Send active router to navigation view
     */
    get navigationView() {
        return this.activeViews[0];
    }

    setupRouter() {
        this.router = new Router();
        Backbone.history.start({
            hashChange: false,
            pushState: true,
            root: '/',
        });
        this.navigationView.data = this.router;
    }
}

(function bootstrap() {
    return new Application();
}());

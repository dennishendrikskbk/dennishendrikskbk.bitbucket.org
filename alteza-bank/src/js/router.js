/* eslint-disable class-methods-use-this */
import { Router as BackboneRouter } from 'backbone';
import $ from 'jquery';

import FormView from './views/form';
import OverviewView from './views/overview';

import HomeTemplate from '../../templates/home.html';
import OverviewTemplate from '../../templates/overview.html';
import PayTemplate from '../../templates/pay.html';

class Router extends BackboneRouter {
    get loadViews() {
        return [
            FormView,
            OverviewView,
        ];
    }

    get routes() {
        return {
            '': 'home',
            'alteza-bank/': 'home',
            'alteza-bank/home': 'home',
            'alteza-bank/overview': 'overview',
            'alteza-bank/pay': 'pay',
        };
    }

    initialize() {
        this.activeViews = [];
    }

    home() {
        // console.log('load home template');
        $('.content-right').html(HomeTemplate);
    }

    overview() {
        // console.log('load overview template');
        $('.content-right').html(OverviewTemplate);
        this.loadView(1);
    }

    pay() {
        // console.log('load pay template');
        $('.content-right').html(PayTemplate);
        this.loadView(0);
    }

    loadView(View) {
        if (!this.activeViews.includes(View)) {
            /* eslint-disable no-new */
            new this.loadViews[View]({
                el: this.el,
                component: this,
            });
            this.activeViews.push(View);
        }
    }
}

export default Router;

/* eslint-disable class-methods-use-this */
import { Collection as BackboneCollection } from 'backbone';
import Store from 'backbone.localstorage';

import TransferModel from '../models/transfer';

class BankTransferCollection extends BackboneCollection {
    get model() {
        return TransferModel;
    }

    get storeName() {
        return 'transfersDB';
    }

    get url() {
        return '/';
    }

    /**
     * Because there is no backend to save data store the data in localStorage
     */
    get localStorage() {
        return new Store('transfers');
    }
}

export default BankTransferCollection;

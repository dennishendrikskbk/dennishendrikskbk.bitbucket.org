/* eslint-disable class-methods-use-this */
import { View as BackboneView } from 'backbone';

import $ from 'jquery';
import 'bootstrap-sass';

class NavigationView extends BackboneView {
    get el() {
        return 'nav';
    }

    get events() {
        return {
            'click a': 'onClickNav',
        };
    }

    set data(value) {
        this.router = value;
    }

    onClickNav(e) {
        e.preventDefault();

        const clickedLink = e.currentTarget.getAttribute('href');

        $(`${this.el} a`).removeClass('active');
        $(e.currentTarget).addClass('active');

        this.router.navigate(clickedLink, { trigger: true });
    }
}

export default NavigationView;

/* eslint-disable class-methods-use-this */
import { View as BackboneView } from 'backbone';
import $ from 'jquery';

import TransferCollection from '../collections/banktransfers';


class NavigationView extends BackboneView {
    get el() {
        return '.content-right';
    }

    /**
     * Get collection on load
     */
    initialize() {
        this.transfers = new TransferCollection();
        this.transfers.fetch();

        this.showTransfers();
    }

    showTransfers() {
        let transferTemplate = '';
        // TODO: Sort collection in reverse order
        this.transfers.each((val, index) => {
            /**
             * To simulate transfer direction for demo
             */
            let way;
            if (index % 2) {
                way = 'from';
            } else {
                way = 'to';
            }

            const item = val.attributes;
            transferTemplate += `
            <li>
                <div class="bank-info">
                    <h5>
                        ${item.naam}
                    </h5>
                    <span class="number">
                        ${item.datum}
                    </span>
                    <span class="way ${way}">
                        Bij
                    </span>
                    <span class="amount">
                        ${item.bedrag}
                    </span>
                </div>
            </li>`;
        });
        $('.js-overview').append(transferTemplate);
        // TODO:  if more than x -> load more button
    }
}

export default NavigationView;

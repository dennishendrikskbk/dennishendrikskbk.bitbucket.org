/* eslint-disable class-methods-use-this */
import { View as BackboneView } from 'backbone';
import $ from 'jquery';

import TransferModel from '../models/transfer';
import TransferCollection from '../collections/banktransfers';

import PayTemplate from '../../../templates/pay.html';


class NavigationView extends BackboneView {
    get el() {
        return '.content-right';
    }

    get events() {
        return {
            submit: 'onSubmitForm',
        };
    }

    initialize() {
        this.transfers = new TransferCollection();
        this.transfers.fetch();
    }

    onSubmitForm(e) {
        e.preventDefault();

        const formData = {};
        $(this.$el.find('form').serializeArray()).each((index, obj) => {
            formData[obj.name] = obj.value;
        });

        console.log(formData);

        const formResult = new TransferModel(formData);

        this.transfers.add(formResult);
        formResult.save(null, { success: () => {
            $('.content-right').html('Betaling gelukt!');
        } });

        this.transfers.trigger('sync');

        // TODO: validate input
    }

    render() {
        this.$el.html(PayTemplate);
    }
}

export default NavigationView;
